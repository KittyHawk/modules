pip install pint

wget -N https://0xacab.org/KittyHawk/KittyHawk/raw/1.5.0dev/encoder.py
wget -N https://0xacab.org/KittyHawk/KittyHawk/raw/1.5.0dev/arsenic_helper.py

export PYTHONIOENCODING=UTF-8
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

for f in *.py
do
    echo Testing: [$f]
    python $f
done
